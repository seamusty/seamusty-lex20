#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "CUnit.h"
#include "Basic.h"

void onTableTest0(void){ 
                         int blockNumber = 1;
                         create(1);
		         int expected = 1; 
			 int actual = onTable(1);  
                         CU_ASSERT_EQUAL( actual, expected );
		       }

void openTest0(void){
                      int blockNumber = 1;
                      create(1);
		      int expected = 1; 
		      int actual = onTable(1);  
                      CU_ASSERT_EQUAL( actual, expected );
                    }

void onTest0(void){
                    int blockNumber = 1;
                    create(1);
		    int expected = 0; 
		    int actual = onTable(1);  
                    CU_ASSERT_EQUAL( actual, expected );
                  }
     

void aboveTest0(void){
                       int blockNumber = 1;
                       create(1);
		       int expected = 0; 
		       int actual = onTable(1);  
                       CU_ASSERT_EQUAL( actual, expected );
 
                     }

void moveTest0(void){}

int init_suite1(void) { return 0; }    /* The suite initialization function. */
int clean_suite1(void) { return 0; }   /* The suite cleanup function. */

/* The main() function for setting up and running the tests.
 *  * Returns a CUE_SUCCESS on successful running, another
 *   * CUnit error code on failure.
 *    */
int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if (
          (NULL == CU_add_test(pSuite, "onTable() test 1", onTableTest0))
       || (NULL == CU_add_test(pSuite, "open() Test 1", openTest0))
       || (NULL == CU_add_test(pSuite, "on() Test 1", onTest0))
       || (NULL == CU_add_test(pSuite, "above() Test 1", aboveTest0))
       || (NULL == CU_add_test(pSuite, "move() Test 1", moveTest0))
      )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}




