OS := $(shell uname -s)

ifeq ($(OS), Darwin)
  CUNIT_PATH_PREFIX = /usr/local/Cellar/cunit/2.1-3/
  CUNIT_DIRECTORY = cunit
endif
ifeq ($(OS), Linux)
  CUNIT_PATH_PREFIX = /util/CUnit/
  CUNIT_DIRECTORY = CUnit/
endif


objects = main.o world.o tests.o
  
CC = gcc
FLAGS = -g -c -Wall -fprofile-arcs -ftest-coverage -std=c11

main : $(objects) main.c
	$(CC) -g -Wall -lm -o main.c $(objects) -lgcov

main.o : main.c
	$(CC) $(FLAGS) main.c -lgcov

tests.o: tests.c
	$(CC) -c $(FLAGS) -I $(CUNIT_PATH_PREFIX)include/$(CUNIT_DIRECTORY) tests.c -lgcov

tests:  tests.o
	gcc -g -Wall -L $(CUNIT_PATH_PREFIX)lib -I $(CUNIT_PATH_PREFIX)includes/$(CUNIT_DIRECTORY) -lm -o tests tests.o -lcunit -lgcov

.PHONY: 
clean :
	rm -fv interpreter tests *.o *.plist *~

